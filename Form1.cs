﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CCalc
{
    public partial class Form1 : Form
    {
        private double result;
        public double Result
        {
            get { return result; }
            set { result = value; }
        }
        

        private bool modifierPressed;

        public bool ModifierPressed
        {
            get { return modifierPressed; }
            set { modifierPressed = value; }
        }

        private char modifier;

        public char Modifier
        {
            get { return modifier; }
            set { modifier = value; }
        }
        private double memory;
        public double Memory
        {
            get { return memory; }
            set { memory = value; }
        }


        public Form1()
        {
            InitializeComponent();
        }

        private void mem_clear_Click(object sender, EventArgs e)
        {
            memory = 0;
        }

        private void one_Click(object sender, EventArgs e)
        {
            if(ModifierPressed)
            {
                ModifierPressed = false;
                input.Clear();
            }
            input.AppendText("1");
        }

        private void two_Click(object sender, EventArgs e)
        {
            if (ModifierPressed)
            {
                ModifierPressed = false;
                input.Clear();
            }
            input.AppendText("2");
        }

        private void three_Click(object sender, EventArgs e)
        {
            if (ModifierPressed)
            {
                ModifierPressed = false;
                input.Clear();
            }
            input.AppendText("3");
        }

        private void four_Click(object sender, EventArgs e)
        {
            if (ModifierPressed)
            {
                ModifierPressed = false;
                input.Clear();
            }
            input.AppendText("4");
        }

        private void five_Click(object sender, EventArgs e)
        {
            if (ModifierPressed)
            {
                ModifierPressed = false;
                input.Clear();
            }
            input.AppendText("5");
        }

        private void six_Click(object sender, EventArgs e)
        {
            if (ModifierPressed)
            {
                ModifierPressed = false;
                input.Clear();
            }
            input.AppendText("6");
        }

        private void seven_Click(object sender, EventArgs e)
        {
            if (ModifierPressed)
            {
                ModifierPressed = false;
                input.Clear();
            }
            input.AppendText("7");
        }

        private void eight_Click(object sender, EventArgs e)
        {
            if (ModifierPressed)
            {
                ModifierPressed = false;
                input.Clear();
            }
            input.AppendText("8");
        }

        private void nine_Click(object sender, EventArgs e)
        {
            if (ModifierPressed)
            {
                ModifierPressed = false;
                input.Clear();
            }
            input.AppendText("9");
        }

        private void zero_Click(object sender, EventArgs e)
        {
            if (ModifierPressed)
            {
                ModifierPressed = false;
                input.Clear();
            }
            input.AppendText("0");
        }

        private void decimaal_Click(object sender, EventArgs e)
        {
            if (!input.Text.Contains("."))
            {
                input.AppendText(".");
            }
        }

        private void input_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void add_Click(object sender, EventArgs e)
        {
            equals_Click(sender, e);
            ModifierPressed = true;
            Modifier = '+';
            result = Convert.ToDouble(input.Text);
        }

        private void subtract_Click(object sender, EventArgs e)
        {
            equals_Click(sender, e);
            ModifierPressed = true;
            Modifier = '-';
            result = Convert.ToDouble(input.Text);
        }

        private void multiply_Click(object sender, EventArgs e)
        {
            equals_Click(sender, e);
            ModifierPressed = true;
            Modifier = '*';
            result = Convert.ToDouble(input.Text);
        }

        private void devide_Click(object sender, EventArgs e)
        {
            equals_Click(sender, e);
            ModifierPressed = true;
            Modifier = '/';
            result = Convert.ToDouble(input.Text);
        }

        private void equals_Click(object sender, EventArgs e)
        {
            //result <modifier> input
            switch (Modifier)
            {
                case '+':
                    Result += Convert.ToDouble(input.Text);
                    break;
                case '-':
                    Result -= Convert.ToDouble(input.Text);
                    break;
                case '/':
                    Result /= Convert.ToDouble(input.Text);
                    break;
                case '*':
                    Result *= Convert.ToDouble(input.Text);
                    break;
                case 'z':
                    Result = 1 / Result;
                    break;
                case 's':
                    Result = Math.Pow(Result, 2);
                    break;
                case 'r':
                    Result = Math.Sqrt(Result);
                    break;
                default:
                    Result = Convert.ToDouble(input.Text);
                    break;
            }
            input.Text = Result.ToString();
            Modifier = ' ';
            ModifierPressed = true;
        }

        private void clear_Click(object sender, EventArgs e)
        {
            Modifier = ' ';
            ModifierPressed = false;
            input.Clear();
            result = 0;
        }

        private void zeropoint_Click(object sender, EventArgs e)
        {
            Modifier = 'z';
            ModifierPressed = true;
            Result = Convert.ToDouble(input.Text);
        }


        private void square_Click(object sender, EventArgs e)
        {
            Modifier = 's';
            ModifierPressed = true;
            Result = Convert.ToDouble(input.Text);
        }

        private void root_Click(object sender, EventArgs e)
        {
            Modifier = 'r';
            ModifierPressed = true;
            Result = Convert.ToDouble(input.Text);
        }

        private void invert_Click(object sender, EventArgs e)
        {
            string temp = input.Text;

            if (!temp.Contains("-"))
            {
                temp = temp.Insert(0, "-");
            }
            else
            {
                temp = temp.Remove(0, 1);
            }

            input.Text = temp;
        }

        private void mem_recall_Click(object sender, EventArgs e)
        {
            input.Text = Convert.ToString(memory);
        }

        private void mem_store_Click(object sender, EventArgs e)
        {
            memory = Convert.ToDouble(input.Text);
            ModifierPressed = true;
        }

    }
}

﻿﻿namespace CCalc
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.input = new System.Windows.Forms.TextBox();
            this.mem_clear = new System.Windows.Forms.Button();
            this.mem_recall = new System.Windows.Forms.Button();
            this.mem_store = new System.Windows.Forms.Button();
            this.procent = new System.Windows.Forms.Button();
            this.root = new System.Windows.Forms.Button();
            this.square = new System.Windows.Forms.Button();
            this.zeropoint = new System.Windows.Forms.Button();
            this.clear = new System.Windows.Forms.Button();
            this.one = new System.Windows.Forms.Button();
            this.two = new System.Windows.Forms.Button();
            this.three = new System.Windows.Forms.Button();
            this.four = new System.Windows.Forms.Button();
            this.five = new System.Windows.Forms.Button();
            this.seven = new System.Windows.Forms.Button();
            this.eight = new System.Windows.Forms.Button();
            this.nine = new System.Windows.Forms.Button();
            this.invert = new System.Windows.Forms.Button();
            this.zero = new System.Windows.Forms.Button();
            this.decimaal = new System.Windows.Forms.Button();
            this.multiply = new System.Windows.Forms.Button();
            this.devide = new System.Windows.Forms.Button();
            this.subtract = new System.Windows.Forms.Button();
            this.add = new System.Windows.Forms.Button();
            this.equals = new System.Windows.Forms.Button();
            this.six = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // input
            // 
            this.input.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.input.Location = new System.Drawing.Point(11, 12);
            this.input.Name = "input";
            this.input.Size = new System.Drawing.Size(133, 20);
            this.input.TabIndex = 0;
            this.input.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.input_KeyPress);
            // 
            // mem_clear
            // 
            this.mem_clear.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mem_clear.Location = new System.Drawing.Point(11, 46);
            this.mem_clear.Name = "mem_clear";
            this.mem_clear.Size = new System.Drawing.Size(39, 20);
            this.mem_clear.TabIndex = 1;
            this.mem_clear.Text = "MC";
            this.mem_clear.UseVisualStyleBackColor = true;
            this.mem_clear.Click += new System.EventHandler(this.mem_clear_Click);
            // 
            // mem_recall
            // 
            this.mem_recall.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mem_recall.Location = new System.Drawing.Point(45, 46);
            this.mem_recall.Name = "mem_recall";
            this.mem_recall.Size = new System.Drawing.Size(36, 20);
            this.mem_recall.TabIndex = 2;
            this.mem_recall.Text = "MR";
            this.mem_recall.UseVisualStyleBackColor = true;
            this.mem_recall.Click += new System.EventHandler(this.mem_recall_Click);
            // 
            // mem_store
            // 
            this.mem_store.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mem_store.Location = new System.Drawing.Point(80, 46);
            this.mem_store.Name = "mem_store";
            this.mem_store.Size = new System.Drawing.Size(29, 20);
            this.mem_store.TabIndex = 5;
            this.mem_store.Text = "MS";
            this.mem_store.UseVisualStyleBackColor = true;
            this.mem_store.Click += new System.EventHandler(this.mem_store_Click);
            // 
            // procent
            // 
            this.procent.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.procent.Location = new System.Drawing.Point(11, 72);
            this.procent.Name = "procent";
            this.procent.Size = new System.Drawing.Size(29, 20);
            this.procent.TabIndex = 6;
            this.procent.Text = "%";
            this.procent.UseVisualStyleBackColor = true;
            // 
            // root
            // 
            this.root.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.root.Location = new System.Drawing.Point(45, 72);
            this.root.Name = "root";
            this.root.Size = new System.Drawing.Size(29, 20);
            this.root.TabIndex = 7;
            this.root.Text = "√x";
            this.root.UseVisualStyleBackColor = true;
            this.root.Click += new System.EventHandler(this.root_Click);
            // 
            // square
            // 
            this.square.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.square.Location = new System.Drawing.Point(80, 72);
            this.square.Name = "square";
            this.square.Size = new System.Drawing.Size(29, 20);
            this.square.TabIndex = 8;
            this.square.Text = "x²";
            this.square.UseVisualStyleBackColor = true;
            this.square.Click += new System.EventHandler(this.square_Click);
            // 
            // zeropoint
            // 
            this.zeropoint.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zeropoint.Location = new System.Drawing.Point(114, 72);
            this.zeropoint.Name = "zeropoint";
            this.zeropoint.Size = new System.Drawing.Size(29, 20);
            this.zeropoint.TabIndex = 9;
            this.zeropoint.Text = "¹/x";
            this.zeropoint.UseVisualStyleBackColor = true;
            this.zeropoint.Click += new System.EventHandler(this.zeropoint_Click);
            // 
            // clear
            // 
            this.clear.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clear.Location = new System.Drawing.Point(114, 46);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(29, 20);
            this.clear.TabIndex = 10;
            this.clear.Text = "C";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // one
            // 
            this.one.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.one.Location = new System.Drawing.Point(11, 97);
            this.one.Name = "one";
            this.one.Size = new System.Drawing.Size(29, 20);
            this.one.TabIndex = 11;
            this.one.Text = "1";
            this.one.UseVisualStyleBackColor = true;
            this.one.Click += new System.EventHandler(this.one_Click);
            // 
            // two
            // 
            this.two.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.two.Location = new System.Drawing.Point(45, 97);
            this.two.Name = "two";
            this.two.Size = new System.Drawing.Size(29, 20);
            this.two.TabIndex = 12;
            this.two.Text = "2";
            this.two.UseVisualStyleBackColor = true;
            this.two.Click += new System.EventHandler(this.two_Click);
            // 
            // three
            // 
            this.three.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.three.Location = new System.Drawing.Point(80, 97);
            this.three.Name = "three";
            this.three.Size = new System.Drawing.Size(29, 20);
            this.three.TabIndex = 13;
            this.three.Text = "3";
            this.three.UseVisualStyleBackColor = true;
            this.three.Click += new System.EventHandler(this.three_Click);
            // 
            // four
            // 
            this.four.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.four.Location = new System.Drawing.Point(11, 122);
            this.four.Name = "four";
            this.four.Size = new System.Drawing.Size(29, 20);
            this.four.TabIndex = 14;
            this.four.Text = "4";
            this.four.UseVisualStyleBackColor = true;
            this.four.Click += new System.EventHandler(this.four_Click);
            // 
            // five
            // 
            this.five.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.five.Location = new System.Drawing.Point(45, 122);
            this.five.Name = "five";
            this.five.Size = new System.Drawing.Size(29, 20);
            this.five.TabIndex = 15;
            this.five.Text = "5";
            this.five.UseVisualStyleBackColor = true;
            this.five.Click += new System.EventHandler(this.five_Click);
            // 
            // seven
            // 
            this.seven.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seven.Location = new System.Drawing.Point(11, 147);
            this.seven.Name = "seven";
            this.seven.Size = new System.Drawing.Size(29, 20);
            this.seven.TabIndex = 17;
            this.seven.Text = "7";
            this.seven.UseVisualStyleBackColor = true;
            this.seven.Click += new System.EventHandler(this.seven_Click);
            // 
            // eight
            // 
            this.eight.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eight.Location = new System.Drawing.Point(45, 147);
            this.eight.Name = "eight";
            this.eight.Size = new System.Drawing.Size(29, 20);
            this.eight.TabIndex = 18;
            this.eight.Text = "8";
            this.eight.UseVisualStyleBackColor = true;
            this.eight.Click += new System.EventHandler(this.eight_Click);
            // 
            // nine
            // 
            this.nine.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nine.Location = new System.Drawing.Point(80, 147);
            this.nine.Name = "nine";
            this.nine.Size = new System.Drawing.Size(29, 20);
            this.nine.TabIndex = 19;
            this.nine.Text = "9";
            this.nine.UseVisualStyleBackColor = true;
            this.nine.Click += new System.EventHandler(this.nine_Click);
            // 
            // invert
            // 
            this.invert.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.invert.Location = new System.Drawing.Point(11, 172);
            this.invert.Name = "invert";
            this.invert.Size = new System.Drawing.Size(29, 20);
            this.invert.TabIndex = 20;
            this.invert.Text = "+/-";
            this.invert.UseVisualStyleBackColor = true;
            this.invert.Click += new System.EventHandler(this.invert_Click);
            // 
            // zero
            // 
            this.zero.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zero.Location = new System.Drawing.Point(45, 172);
            this.zero.Name = "zero";
            this.zero.Size = new System.Drawing.Size(29, 20);
            this.zero.TabIndex = 21;
            this.zero.Text = "0";
            this.zero.UseVisualStyleBackColor = true;
            this.zero.Click += new System.EventHandler(this.zero_Click);
            // 
            // decimaal
            // 
            this.decimaal.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.decimaal.Location = new System.Drawing.Point(80, 172);
            this.decimaal.Name = "decimaal";
            this.decimaal.Size = new System.Drawing.Size(29, 20);
            this.decimaal.TabIndex = 22;
            this.decimaal.Text = ",";
            this.decimaal.UseVisualStyleBackColor = true;
            this.decimaal.Click += new System.EventHandler(this.decimaal_Click);
            // 
            // multiply
            // 
            this.multiply.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.multiply.Location = new System.Drawing.Point(114, 122);
            this.multiply.Name = "multiply";
            this.multiply.Size = new System.Drawing.Size(29, 20);
            this.multiply.TabIndex = 23;
            this.multiply.Text = "X";
            this.multiply.UseVisualStyleBackColor = true;
            this.multiply.Click += new System.EventHandler(this.multiply_Click);
            // 
            // devide
            // 
            this.devide.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.devide.Location = new System.Drawing.Point(114, 97);
            this.devide.Name = "devide";
            this.devide.Size = new System.Drawing.Size(29, 20);
            this.devide.TabIndex = 24;
            this.devide.Text = "/";
            this.devide.UseVisualStyleBackColor = true;
            this.devide.Click += new System.EventHandler(this.devide_Click);
            // 
            // subtract
            // 
            this.subtract.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subtract.Location = new System.Drawing.Point(114, 147);
            this.subtract.Name = "subtract";
            this.subtract.Size = new System.Drawing.Size(29, 20);
            this.subtract.TabIndex = 25;
            this.subtract.Text = "-";
            this.subtract.UseVisualStyleBackColor = true;
            this.subtract.Click += new System.EventHandler(this.subtract_Click);
            // 
            // add
            // 
            this.add.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.add.Location = new System.Drawing.Point(114, 172);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(29, 20);
            this.add.TabIndex = 26;
            this.add.Text = "+";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // equals
            // 
            this.equals.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equals.Location = new System.Drawing.Point(11, 198);
            this.equals.Name = "equals";
            this.equals.Size = new System.Drawing.Size(132, 24);
            this.equals.TabIndex = 27;
            this.equals.Text = "=";
            this.equals.UseVisualStyleBackColor = true;
            this.equals.Click += new System.EventHandler(this.equals_Click);
            // 
            // six
            // 
            this.six.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.six.Location = new System.Drawing.Point(80, 122);
            this.six.Name = "six";
            this.six.Size = new System.Drawing.Size(29, 20);
            this.six.TabIndex = 16;
            this.six.Text = "6";
            this.six.UseVisualStyleBackColor = true;
            this.six.Click += new System.EventHandler(this.six_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(155, 233);
            this.Controls.Add(this.equals);
            this.Controls.Add(this.add);
            this.Controls.Add(this.subtract);
            this.Controls.Add(this.devide);
            this.Controls.Add(this.multiply);
            this.Controls.Add(this.decimaal);
            this.Controls.Add(this.zero);
            this.Controls.Add(this.invert);
            this.Controls.Add(this.nine);
            this.Controls.Add(this.eight);
            this.Controls.Add(this.seven);
            this.Controls.Add(this.six);
            this.Controls.Add(this.five);
            this.Controls.Add(this.four);
            this.Controls.Add(this.three);
            this.Controls.Add(this.two);
            this.Controls.Add(this.one);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.zeropoint);
            this.Controls.Add(this.square);
            this.Controls.Add(this.root);
            this.Controls.Add(this.procent);
            this.Controls.Add(this.mem_store);
            this.Controls.Add(this.mem_recall);
            this.Controls.Add(this.mem_clear);
            this.Controls.Add(this.input);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox input;
        private System.Windows.Forms.Button mem_clear;
        private System.Windows.Forms.Button mem_recall;
        private System.Windows.Forms.Button mem_store;
        private System.Windows.Forms.Button procent;
        private System.Windows.Forms.Button root;
        private System.Windows.Forms.Button square;
        private System.Windows.Forms.Button zeropoint;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Button one;
        private System.Windows.Forms.Button two;
        private System.Windows.Forms.Button three;
        private System.Windows.Forms.Button four;
        private System.Windows.Forms.Button five;
        private System.Windows.Forms.Button seven;
        private System.Windows.Forms.Button eight;
        private System.Windows.Forms.Button nine;
        private System.Windows.Forms.Button zero;
        private System.Windows.Forms.Button invert;
        private System.Windows.Forms.Button decimaal;
        private System.Windows.Forms.Button multiply;
        private System.Windows.Forms.Button devide;
        private System.Windows.Forms.Button subtract;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.Button equals;
        private System.Windows.Forms.Button six;
    }
}